import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

import { authRoutes as auth } from '../components/views/Auth'
import { todoListRoutes as todoList } from '../components/views/TodoList'

const routes = [ ...auth, ...todoList ];

const router = new Router({
  mode: 'history',
  routes
});
  
export default router