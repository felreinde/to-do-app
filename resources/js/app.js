import Vue from 'vue'
import App from './components/App'
import router from './config/routes.js'
import axios from './config/axios.js'

Vue.prototype.$http = axios

const app = new Vue({
    el: '#app',
    components: { App },
    router
})
