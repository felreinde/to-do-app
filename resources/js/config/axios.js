import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:8000/api'
  })

instance.interceptors.request.use(config => {
  
  config.headers['Content-Type'] = 'application/json'
//   config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('todoApp.jwt')
  return config
})

instance.interceptors.response.use(response => {
  return response
})

export default instance
