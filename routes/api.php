<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('todo-list', 'TodoListController@store');
Route::get('todo-list', 'TodoListController@index');
Route::put('todo-list/{id}', 'TodoListController@update');
Route::delete('todo-list/{id}', 'TodoListController@destroy');

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');