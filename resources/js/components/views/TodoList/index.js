import TodoList from './ToDoList.vue'

const todoListRoutes = [
    {
        path: '/',
        name: 'landingpage',
        component: TodoList
    }
];

export { todoListRoutes }
